# Kubernetes Deployment #

1. Build container image

    ``` sh
    docker build -t rsharma/inventory-service
    ```
2. Create container repository of name <your dockerhub id here>/inventory-service at Docker Hub
3. Login & push to docker hub

    ``` sh
    docker login
    # provide your credentials
    docker push rsharma/inventory-service
    ```

4. Configure kubectl

    ```sh
    mv <namespace>-kubeconfig ~/.kube/config
    kubectl get all
    ```

5. Run the inventory service by creating a new deployment

    ``` sh
    kubectl create deployment inventory-service \
        --image=rsharma/inventory-service
    ```

    Alternatively you can also create deployment.yml which can be used to create the deployment using

    ``` sh
    kubectl create -f deployment.yml
    ```

    Where the deployment.yml should have the following contents
    
    ```yaml
    apiVersion: extensions/v1beta1
    kind: Deployment
    metadata:
      labels:
        app: inventory-service
      name: inventory-service
      namespace: rama244050
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: inventory-service
      template:
        metadata:
          labels:
            app: inventory-service
        spec:
          containers:
          - image: rsharma/inventory-service
            imagePullPolicy: Always
            name: inventory-service
    ``` 

6. Expose inventory service

    ``` sh
    kubectl expose deployment/inventory-service \
        --port 8001 --target-port=8001 \
        --name=inventory-service
    ```
    
   Alternatively you can create a yaml file with the following contents and creat the service
    
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      labels:
        app: inventory-service
      name: inventory-service
      namespace: <your namespace here>
    spec:
      ports:
      - port: 8001
        protocol: TCP
        targetPort: 8001
      selector:
        app: inventory-service
      type: ClusterIP
    ```

7. Get your namespace name

    ``` sh
    kubectl config view -o 'jsonpath={.current-context}'
    ```

8. The inventory service will accessible from `rama244050.wcloudacademy.com/skus`

    **NOTE**

    Please note that the deployed service will be accessible
    via the ingress URL `<namespace>.wcloudacademy.com/*` only if the
    inventory-service and catalog-service kubernetes service resource
    uses the same name and exposes the same port

    | Service   | Kubernetes Service Name | Kubernetes `port` & `targetPort` |
    |-----------|-------------------------|----------------------------------|
    | Inventory | `inventory-service`     | `8001`                           |
    | Catalog   | `catalog-service`       | `8002`                           |


9. Access the application via the ingress url

    ``` sh
    INGRESS_HOST=<namespace>.wcloudacademy.com
    # get skus
    curl http://${INGRESS_HOST}/skus
    # create a sku
    curl -XPOST -H 'Content-Type: application/json' \
    -d '{"productId":1,"name":"one","description":"this is sku number one","price":3.5,"count":10}' \
    http://${INGRESS_HOST}/skus
    ```

10. Run the catalog service

    ``` sh
    kubectl create deployment catalog-service \
        --image=<your dockerhub id here>/catalog-service
    ```

    Alternatively you can use the service.yml

    ``` sh
    kubectl create -f service.yml
    ```

11. Expose catalog service

    ``` sh
    kubectl expose deployment/catalog-service \
        --port 8002 --target-port=8002 \
        --name=catalog-service
    ```

    The url for the inventory service will be `<namespace>.wcloudacademy.com/products`

12. The acutator endpoints are also exposed from the ingress URL 
   `<namespace>.wcloudacademy.com/actuator/*`

13. Cleanup all resources once you are done

    ``` sh
    for f in inventory catalog; do
      kubectl delete deploy/$f-service service/$f-service
    done
    ```
