package com.wipro.catalog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

@Configuration
public class ProductcatalogMetricsConfiguration {
	@Bean
    public Counter createdProduct(MeterRegistry registry) {
        return Counter
            .builder("catalog.products")
            .description("Number of products created")
            .register(registry);
    }

}