package com.wipro.catalog;

import com.google.common.base.Strings;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@EnableScheduling
@Controller
public class ProductController {
	 

	private ProductcatalogMetricsConfiguration metric;

    @Autowired
    @LoadBalanced
    RestTemplate restTemplate;
    @Autowired
    private MeterRegistry meterRegistry;
    

    static AtomicInteger ID = new AtomicInteger();
    List<Product> products = new ArrayList<>();
    



    @GetMapping(path = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> getProducts() {
        if (!products.isEmpty()) {
            return new ResponseEntity<>(products, HttpStatus.OK);
        }
        return new ResponseEntity<>(Collections.EMPTY_LIST, HttpStatus.OK);

    }
    
   

    @PostMapping(path = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addProduct(@RequestBody Product newProduct) {
    	
		
    	
        if (newProduct != null) {
            if (!Strings.isNullOrEmpty(newProduct.getCatalog())) {
                newProduct.setId(ID.incrementAndGet());
                products.add(newProduct);
                return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("catalog mandatory", HttpStatus.BAD_REQUEST);
            }
        }
       
		
        
        return new ResponseEntity<>("payload empty", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProduct(@PathVariable("id") Integer id) {

        ProductDetail productDetail = null;
        /*
        ProductDetail pd = products.stream().filter((product1) -> {
           if(id.equals(product1.getId()) ) {
                productDetail = new ProductDetail();
                productDetail.setSkus(getSKUs(product1.getId()));
                return productDetail;
           }
           return null;
        }).findAny().orElseThrow(() -> new ProductNotFoundException(id));
        */

        for (Product product : products) {
            if (id.equals(product.getId())) {
                productDetail = new ProductDetail(product);
                productDetail.setSkus(getSKUs(product.getId()));
                return new ResponseEntity<>(productDetail, HttpStatus.OK);
            }
        }

        if (productDetail == null) {
            throw new ProductNotFoundException(id);
        }

        return null;
    }

    public List<SKU> getSKUs(Integer productId) {
        List<SKU> skuList = restTemplate.getForEntity("https://inventory-service/skus?productId=" + productId, List.class).getBody();
        if (skuList != null) {
            return skuList;
        }
        return Collections.emptyList();
    }

    @PutMapping(path = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateProduct(@PathVariable("id") Integer id, @Valid @RequestBody Product updatedProduct) {
        Product product = products.stream().filter(product1 -> id.equals(product1.getId())).findAny().orElseThrow(() -> new ProductNotFoundException(id));
        product.setName(updatedProduct.getName());
        product.setCatalog(updatedProduct.getCatalog());
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @DeleteMapping(path = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Integer id) {
        Product product = products.stream().filter(product1 -> id.equals(product1.getId())).findAny().orElseThrow(() -> new ProductNotFoundException(id));
        products.remove(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

}
