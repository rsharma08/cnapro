package com.wipro.inventory.inventoryservice;

import com.wipro.inventory.inventoryservice.SKU;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SKUControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;


    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();
        assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }

    @SuppressWarnings("unchecked")
    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


    @Test
    public void test01GetSKUSWithEmptyList() throws Exception {
        mockMvc.perform(get("/skus")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"))
                .andExpect(jsonPath("$.length()").value(0))
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    public void test02AddSKUsToList() throws Exception {
        SKU sku = new SKU();
        sku.setProductId(100);
        sku.setName("Nike");
        sku.setDescription("Nike running Shoes");
        sku.setPrice(5000.00);
        sku.setCount(100);

        mockMvc.perform(post("/skus").contentType(MediaType.APPLICATION_JSON)
                .content(json(sku)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.productId").value(100))
                .andExpect(jsonPath("$.name").value("Nike"))
                .andExpect(jsonPath("$.description").value("Nike running Shoes"))
                .andExpect(jsonPath("$.price").value(5000.00))
                .andExpect(jsonPath("$.count").value(100))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test02AddSKUsToList_ProductId_is_Null() throws Exception {
        SKU sku = new SKU();
        sku.setName("Nike");
        sku.setDescription("Nike running Shoes");
        sku.setPrice(5000.00);
        sku.setCount(100);

        mockMvc.perform(post("/skus").contentType(MediaType.APPLICATION_JSON)
                .content(json(sku)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Product Id mandatory"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test02AddSKUsToList_Invalid_Payload() throws Exception {
        final String jsonString = "[{\"foo\": \"bar\"},{\"foo\": \"biz\"}]";

        mockMvc.perform(post("/skus").contentType(MediaType.APPLICATION_JSON)
                .content((jsonString)))
                .andExpect(status().isBadRequest())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test03GetSKUsWithId() throws Exception {

        SKU sku2 = new SKU();
        sku2.setProductId(200);
        sku2.setName("Addidas");
        sku2.setDescription("Addidas running Shoes");
        sku2.setPrice(5500.00);
        sku2.setCount(300);

        mockMvc.perform(post("/skus").contentType(MediaType.APPLICATION_JSON)
                .content(json(sku2)));


        mockMvc.perform(get("/skus/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":2,\"productId\":200,\"name\":\"Addidas\",\"description\":\"Addidas running Shoes\",\"price\":5500.0,\"count\":300}"))
                //.andExpect(jsonPath("$.length()").value(1))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public  void test04GetSKUsWithID_IDDoesNotExist() throws Exception {
        mockMvc.perform(get("/skus/3")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string("SKU not found"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test05updateSKU_valid() throws Exception {
        SKU updateSku = new SKU();
        updateSku.setName("Addidas Updated");
        updateSku.setDescription("Addidas NEO running Shoes");
        updateSku.setCount(900);
        updateSku.setPrice(9000.00);
        updateSku.setProductId(200);

        mockMvc.perform(put("/skus/2").contentType(MediaType.APPLICATION_JSON)
                .content(json(updateSku)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.productId").value(200))
                .andExpect(jsonPath("$.name").value("Addidas Updated"))
                .andExpect(jsonPath("$.description").value("Addidas NEO running Shoes"))
                .andExpect(jsonPath("$.price").value(9000.00))
                .andExpect(jsonPath("$.count").value(900))
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    public void test06updateSKU_SKU_NOT_FOUND() throws Exception {
        SKU updateSku = new SKU();
        updateSku.setName("Addidas Updated");
        updateSku.setDescription("Addidas NEO running Shoes");
        updateSku.setCount(900);
        updateSku.setPrice(9000.00);

        mockMvc.perform(put("/skus/3").contentType(MediaType.APPLICATION_JSON)
                .content(json(updateSku)))
                .andExpect(status().isNotFound())
                .andExpect(content().string("SKU not found"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test07updateSKU_Not_Valid() throws Exception {
        SKU updateSku = new SKU();
        updateSku.setId(4);


        mockMvc.perform(put("/skus/id=2").contentType(MediaType.APPLICATION_JSON)
                .content(json(updateSku)))
                .andExpect(status().isBadRequest())
                //.andExpect(content().string("Invalid SKU payload"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test08deleteSKU_valid() throws Exception {
        mockMvc.perform(delete("/skus/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test09deleteSKU_Not_Found_Valid() throws Exception {
        mockMvc.perform(delete("/skus/3")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string("SKU not found"))
                .andDo(MockMvcResultHandlers.print());
    }
}