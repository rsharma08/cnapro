package com.wipro.inventory.inventoryservice;

public class SKUNotFoundException extends RuntimeException {

    private final Integer id;

    public SKUNotFoundException(final Integer id) {
        super("SKU not found");
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
