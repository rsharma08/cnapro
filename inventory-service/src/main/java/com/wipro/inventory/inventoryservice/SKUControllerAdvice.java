package com.wipro.inventory.inventoryservice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class SKUControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SKUNotFoundException.class)
    public ResponseEntity<?> notFoundException(final SKUNotFoundException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> assertionException(final IllegalArgumentException e) {
        return new ResponseEntity<>("Invalid SKU payload", HttpStatus.BAD_REQUEST);
    }
}
