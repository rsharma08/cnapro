package com.wipro.inventory.inventoryservice;

<<<<<<< HEAD
=======

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
>>>>>>> 1cd2c1274f19ebdea82734282bf00e464db0bf6a
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

<<<<<<< HEAD
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController

public class SKUController {
	static AtomicInteger ID = new AtomicInteger();
	List<SKU> skus = new ArrayList<>();
	
	@GetMapping ("/skus")
	public List<SKU> getSKUs() {
		return Collections.emptyList();
	}
	
	@PostMapping("/skus")
	public SKU addSKU(@RequestBody SKU newsku) {
		newsku.setId(ID.getAndIncrement());
		skus.add(newsku);
		
		return newsku;
		
	}

=======
@RestController
public class SKUController {

    static AtomicInteger ID = new AtomicInteger();
    List<SKU> skus = new ArrayList<>();

    @GetMapping(path = "/skus", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SKU>> getSKUs() {
        if (!skus.isEmpty()) {
            return new ResponseEntity<>(skus, HttpStatus.OK);
        }
        return new ResponseEntity<>(Collections.EMPTY_LIST, HttpStatus.OK);

    }

    @PostMapping(path = "/skus")
    public ResponseEntity<?> addSKU(@RequestBody SKU newSKU) {
        if (newSKU != null) {
            if (newSKU.getProductId() != null) {
                newSKU.setId(ID.incrementAndGet());
                skus.add(newSKU);
                return new ResponseEntity<>(newSKU, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("productId is mandatory", HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<>("payload empty", HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = "/skus/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSKU(@PathVariable("id") Integer id) {

        SKU sku = skus.stream().filter(sku1 -> id.equals(sku1.getId())).findAny().orElseThrow(() -> new SKUNotFoundException(id));
        return new ResponseEntity<>(sku, HttpStatus.OK);
    }

    @PutMapping(path = "/skus/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateSKU(@PathVariable("id") Integer id, @Valid @RequestBody SKU updatedSKU) {
        SKU sku = skus.stream().filter(sku1 -> id.equals(sku1.getId())).findAny().orElseThrow(() -> new SKUNotFoundException(id));
        sku.setProductId(updatedSKU.getProductId());
        sku.setName(updatedSKU.getName());
        sku.setDescription(updatedSKU.getDescription());
        sku.setPrice(updatedSKU.getPrice());
        sku.setPrice(updatedSKU.getPrice());
        sku.setCount(updatedSKU.getCount());
        return new ResponseEntity<>(sku, HttpStatus.OK);
    }

    @DeleteMapping(path = "/skus/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteSKU(@PathVariable("id") Integer id) {
        SKU sku = skus.stream().filter(sku1 -> id.equals(sku1.getId())).findAny().orElseThrow(() -> new SKUNotFoundException(id));
        skus.remove(sku);
        return new ResponseEntity<>(sku, HttpStatus.OK);
    }
>>>>>>> 1cd2c1274f19ebdea82734282bf00e464db0bf6a
}
